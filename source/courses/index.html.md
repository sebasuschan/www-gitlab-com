---
layout: markdown_page
title: Courses
---



This page has been deprecated. As noted in [our handbook guidlines](/handbook/handbook-usage/#guidelines) we want to organize per function and result, not per format. You can find courses listed on the appropriate functional group's handbook page(s).
