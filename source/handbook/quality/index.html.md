---
layout: markdown_page
title: "Quality"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

- [**Public Issue Tracker (for GitLab CE)**](https://gitlab.com/gitlab-org/gitlab-ce);
  please use confidential issues for topics that should only be visible to team members at GitLab.
- Chat channels; please use chat channels for questions that don't seem appropriate to use the issue tracker for.
  - [#qa](https://gitlab.slack.com/archives/qa): QA pipelines post into this
    channel, QA engineers should monitor this channel to act on alerts. "Acting
    on" may be remediating or just fixing noisy alerts.

## Quality goals and team(s)

### OKRs

See [https://about.gitlab.com/okrs/](https://about.gitlab.com/okrs/)

### High level long-term goals

* Stabilize & improve the release process
  * What should we be checking prior to each release, when & who is responsible
  * When and how should automated tests be run
  * Learn from common or frequent mistakes & regressions
* Improve [GitLab QA] test coverage, reliability and efficiency
  * Run-time, de-duplication
  * Which tests should run when
  * How can we put test result data in front of the right people & have them act on it
* Gain insight into development & test metrics
  * See the [gitlab-insights project](https://gitlab.com/gitlab-org/gitlab-insights)
* Improvements to the development process
  * Unit test coverage
  * CE/EE maintenance
  * Developers contributing to [GitLab QA] scenarios
  * Integrate feedback from Sales & Customer Support

### Teams in Quality

- **[Edge](edge)**: Improving the development process, and codebase quality/maintenance
- **QA / Test Automation**: Helping to improve the quality of the product with end-to-end automated testing

## Test Automation

The GitLab test automation framework is distributed across two projects: 
* GitLab QA, the test orchestration tool
* The scenarios and spec files within the GitLab main codebase.

### Architecture overview

### Installation & Execution

* Install and set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
* Install and run [GitLab QA] to kick off test execution. See the README for more information on how to run the test framework.
  * The spec files (test cases) can be found in the [GitLab main codebase](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/qa)

## Development process quality

* Merge request checklist
  * The default template is saved at the project level and can be viewed/edited in Settings -> Repository 
  * There are also [specific templates for Database and Documentation changes](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/.gitlab/merge_request_templates)

## Release process overview

The release process follows [this set of templates](https://gitlab.com/gitlab-org/release-tools/tree/master/templates). Additional supporting docs about the release process can be found [here](https://gitlab.com/gitlab-org/release/docs/).

* First Deployable RC
    * Automated tests are run against Staging and Canary
    * An Issue is created listing the QA tasks for this release candidate
        * The items from the Release Kickoff Doc are copied to the Issue and associated with the appropriate PM
        * The PM performs testing on staging and lists any issues found
        * Other changes related to bugs and improvements are gathered from a Git comparison of this release from the previous one
        * The Engineering teams confirms that these other changes come with tests or have been tested on staging
    * After 24 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production
    * Example: [10.6 RC1 QA Task](https://gitlab.com/gitlab-org/release/tasks/issues/116)
* Subsequent RCs
    * Automated tests are run against Staging and Canary
    * An issue is created listing the QA tasks for this release candidate
        * The changes that had been merged in since the previous release are captured in a QA task Issue, associated with the appropriate engineer
        * The engineer performs testing on staging, or confirms that the change came with automated tests that are passing
    * After 12 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production
    * Example: [10.6 RC6 QA Task](https://gitlab.com/gitlab-org/release/tasks/issues/135)
* Final Release
    * Automated tests are run against Staging and Canary as a sanity check
    * Since no changes should have been included between the last RC and the release-day build, no additional testing or review should be required.
    * The final release candidate is deployed to production

## Recruiting
* Information regarding the technical quizzes and assignments that are sent to candidates can be found [here](https://gitlab.com/gitlab-com/people-ops/applicant-questionnaires/blob/master/automation-engineer.md) (GITLAB ONLY)
* The steps of the hiring process can be found [here](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/engineering.md) (GITLAB ONLY)

## Other Related Pages

- [Engineering](/handbook/engineering)
- [Issue Triage](issue-triage)
- [Issue Triage Policies](/handbook/engineering/issue-triage)
- [Performance of GitLab](/handbook/engineering/performance)
- [Monitoring of GitLab.com](/handbook/infrastructure/monitoring)
- [Production Readiness Guide](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md)

[GitLab QA]: https://gitlab.com/gitlab-org/gitlab-qa
