---
layout: markdown_page
title: Glossary and Terminology
---

**The page has been moved to the new [GitLab University glossary](https://docs.gitlab.com/ee/university/glossary/) page**
